package com.example.mjoss.motionaudiokt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.hardware.Sensor.TYPE_ROTATION_VECTOR
import android.hardware.SensorManager
import android.hardware.SensorManager.SENSOR_DELAY_UI
import android.media.AudioManager.ACTION_AUDIO_BECOMING_NOISY
import android.media.MediaMetadataRetriever
import android.media.MediaMetadataRetriever.*
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_player.*

class PlayerActivity : AppCompatActivity() {

    lateinit private var mediaPlayer: MediaPlayer
    lateinit private var audioManipulator: AudioManipulator
    lateinit private var sensorManager: SensorManager
    private var locked = false
    private val musicStopper = MusicStopper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)

        resetButton.setOnClickListener { audioManipulator.reset() }

        lockButton.setOnClickListener {
            if (locked) {
                unlock()
            } else {
                lock()
            }
        }

        controlButton.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                pause()
            } else {
                play()
            }
        }

        showTrackInfo()

        mediaPlayer = MediaPlayer.create(this, intent.data)
        audioManipulator = AudioManipulator(mediaPlayer, { speed, pitch ->
            textViewSpeed.text = getString(R.string.ui_speed, speed)
            textViewPitch.text = getString(R.string.ui_pitch, pitch)
        })

        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager
        unlock()
        play()
    }


    override fun onPause() {
        super.onPause()
        lock()
        pause()
        unregisterReceiver(musicStopper)
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter(ACTION_AUDIO_BECOMING_NOISY)
        registerReceiver(musicStopper, filter)
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.release()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        intent.setClass(this, PickActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showTrackInfo() {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(this, intent.data)
        songTextView.text = retriever.extractMetadata(METADATA_KEY_TITLE)
        artistTextView.text = retriever.extractMetadata(METADATA_KEY_ARTIST)
        albumTextView.text = retriever.extractMetadata(METADATA_KEY_ALBUM)
        val bytes = retriever.embeddedPicture
        if (bytes != null) {
            val image = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
            albumImageView.setImageBitmap(image)
        }
    }

    private fun play() {
        mediaPlayer.start()
        controlButton.setImageResource(android.R.drawable.ic_media_pause)
        lockButton.isEnabled = true
    }

    private fun pause() {
        mediaPlayer.pause()
        controlButton.setImageResource(android.R.drawable.ic_media_play)
        lock()
        lockButton.isEnabled = false
    }

    private fun lock() {
        sensorManager.unregisterListener(audioManipulator)
        lockButton.text = getString(R.string.ui_unlock)
        locked = true
    }

    private fun unlock() {
        if (mediaPlayer.isPlaying) {
            sensorManager.registerListener(audioManipulator, sensorManager.getDefaultSensor(TYPE_ROTATION_VECTOR), SENSOR_DELAY_UI)
            lockButton.text = getString(R.string.ui_lock)
            locked = false
        }
    }

    private inner class MusicStopper : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (intent.action == ACTION_AUDIO_BECOMING_NOISY) {
                pause()
                lock()
            }
        }

    }
}
