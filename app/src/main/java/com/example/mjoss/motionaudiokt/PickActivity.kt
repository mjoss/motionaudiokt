package com.example.mjoss.motionaudiokt

import android.content.Intent
import android.content.Intent.ACTION_GET_CONTENT
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

private const val CODE_PICK_TRACK = 1

class PickActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val pickIntent = Intent(ACTION_GET_CONTENT)
        pickIntent.type = "audio/*"
        startActivityForResult(pickIntent, CODE_PICK_TRACK)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == CODE_PICK_TRACK) {
            data.setClass(this, PlayerActivity::class.java)
            finish()
            startActivity(data)
        }
    }
}
