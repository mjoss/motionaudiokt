package com.example.mjoss.motionaudiokt

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.media.MediaPlayer
import android.media.PlaybackParams

private const val X = 0
private const val Y = 1
private const val Z = 2

/**
 * Changes pitch and speed of  MediaPlayer, using Rotation Vector sensor data.
 *
 * @see android.media.MediaPlayer
 * @see android.media.PlaybackParams
 * @param mediaPlayer MediaPlayer which will be used.
 * @param changeListener Function to execute when playback is changed.
 * */
class AudioManipulator(val mediaPlayer: MediaPlayer,
                       val changeListener: ((speed: Float, pitch: Float) -> Unit) = { _, _ -> })
    : SensorEventListener {

    init {
        reset()
    }

    /**
     * The "neutral point" of the sensor, used to calculate deltas.
     *
     * @see offsetValues
     */
    private var neutralValues: FloatArray? = null
    /**
     * The Delta between neutralValues and the sensor values.
     *
     * @see neutralValues
     */
    private val offsetValues = FloatArray(3)

    /**
     * Changes the PlaybackParams according to Rotation Vector data.
     * @see PlaybackParams
     */
    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ROTATION_VECTOR) {
            if (neutralValues != null) {
                for (i in offsetValues.indices) {
                    offsetValues[i] = event.values[i] - neutralValues!![i]
                }

                val params = mediaPlayer.playbackParams
                var changed = false
                if (Math.abs(offsetValues[X]) >= 0.1) {
                    params.speed = offsetValues[X] + 1
                    changed = true
                } else {
                    params.speed = 1f
                }


                if (Math.abs(offsetValues[Y])  >= 0.1) {
                    params.pitch = offsetValues[Y] + 1
                    changed = true
                } else {
                    params.pitch = 1f
                }

                if (changed) {
                    val wasPaused = !mediaPlayer.isPlaying

                    changeListener(params.speed, params.pitch)
                    mediaPlayer.playbackParams = params
                    if(wasPaused){
                        mediaPlayer.pause()
                    }
                }
            } else {
                neutralValues = event.values.copyOf()
            }
        }
    }

    /**
     * Resets pitch and speed to 1.0 and sets current position as neutral position.
     */
    fun reset() {
        mediaPlayer.playbackParams = PlaybackParams().allowDefaults()
        changeListener(1f, 1f)
        neutralValues = null
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
}